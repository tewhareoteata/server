# server

## Server used at *Te Whare o te Ata*.

### Introduction

For many years a Server has been part of the Information Technology used at 
Te Whare o te Ata. The role that the server has been required to perform has 
changed over time to meet whatever were perceived as the current needs.

Currently the function of the server to be a router for traffic from the 
main 10.1.x.x network to the 10.2.x.x and 10.3.x.x network. 

The servers 10.2 network is a 1Gb/s mini-PCIe ethernet adapter in the server 
linked to a faceplate in the lounge.

The servers 10.3 network is a 1Gb/s PCI ethernet adapter in the server linked 
to a faceplate in the outer building.



### Notes:

Initial Documentation: 2019-04-06

Author: Ian Stewart
